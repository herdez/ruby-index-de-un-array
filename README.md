## Ejercicio - Index de un array

Define el método `get_index` que recibe un arreglo de números y regresa el valor y el índice de cada uno de los elementos en un array.

Se entregarán dos versiones de este programa, la primera versión con estructura iterativa `each` y la otra solamente con `métodos enumerables`.


```ruby
#get_index method



#Driver code
p get_index([2, 10, 6, 34, 0, 3]) == [[2, 0], [10, 1], [6, 2], [34, 3], [0, 4], [3, 5]]
```

